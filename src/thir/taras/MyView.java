package thir.taras;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyView {

  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Scanner input = new Scanner(System.in);

  Locale locale;
  ResourceBundle bundle;

  private void setMenu() {

    menu = new LinkedHashMap<>();
    menu.put("1", bundle.getString("1"));
    menu.put("2", bundle.getString("2"));
    menu.put("3", bundle.getString("3"));
    menu.put("4", bundle.getString("4"));
    menu.put("5", bundle.getString("5"));
    menu.put("6", bundle.getString("6"));
    menu.put("Q", bundle.getString("Q"));
  }

  public MyView() {
    locale = new Locale("uk");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    methodsMenu = new LinkedHashMap<>();

    methodsMenu.put("1", this::testStringUtils);
    methodsMenu.put("2", this::internationalizeMenuUkrainian);
    methodsMenu.put("3", this::internationalizeMenuEnglish);
    methodsMenu.put("4", this::internationalizeMenuJapan);
    methodsMenu.put("5", this::internationalizeMenuArabian);
    methodsMenu.put("6", this::testRegEx);
  }

  private void testStringUtils() {
    StringUtils utils = new StringUtils();
    utils.addToParameters(11,33,"null",null,Optional.empty());
    System.out.println(utils.concat());
  }

  private void internationalizeMenuUkrainian() {
    locale = new Locale("uk");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    show();
  }

  private void internationalizeMenuEnglish() {
    locale = new Locale("en");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    show();
  }
  private void internationalizeMenuJapan() {
	    locale = new Locale("jp");
	    bundle = ResourceBundle.getBundle("MyMenu", locale);
	    setMenu();
	    show();
	  }
  private void internationalizeMenuArabian() {
	    locale = new Locale("eg");
	    bundle = ResourceBundle.getBundle("MyMenu", locale);
	    setMenu();
	    show();
	  }

  private void testRegEx() {
      String message = "Hello, I'm Taras. Nice to see you";
      System.out.println(message);
      String message2 = message.replaceAll("[aeiouAEIOU]", "_");
      System.out.println(message2);
      String[] mm = message.split("the|you4");
        for (String str : mm) {
            System.out.println(str);
    }

      Pattern pattern = Pattern.compile("H+.");
      Matcher matcher = pattern.matcher(message);

      while (matcher.find()) {

          System.out.print(message
                  .substring(matcher.start(), matcher.end()) + "*");

          System.out.println(matcher.matches());
      }
  }

  //-------------------------------------------------------------------------

  private void outputMenu() {
    System.out.println("\nMY INTERNATIONAL MENU:");
    for (String key : menu.keySet()) {
      if (key.length() == 1) {
        System.out.println(menu.get(key));
      }
    }
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please, select menu point.");
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("Q"));
  }
}
